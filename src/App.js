// import { Fragment } from 'react';
import {useState, useEffect} from 'react';
// import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import AddProduct from './pages/AddProduct'
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout'
import Register from './pages/Register';
import Error from './pages/Error';
import Profile from './pages/Profile';
import OrderHistory from './pages/OrderHistory';
import AddToCart from './pages/AddToCart';
// import CartView from './pages/CartView';




import './App.css';
import { UserProvider } from './UserContext';

function App() {




  // This state hook is for the user state that defined here for the a global.
  const [user, setUser] = useState({
      id: null,
      isAdmin: null
  });

  // Function for clearing the localStorage to logout the user.
  const unsetUser = () => {
    localStorage.clear();
  }

  //Because our user state's values are reset to null every time the user reloads the page (thus logging the user out), we want to use React's useEffect hook to fetch the logged-in user's details when the page is reloaded. By using the token saved in localStorage when a user logs in, we can fetch the their data from the database, and re-set the user state values back to the user's details.
    useEffect(() => {

        fetch(`http://localhost:4000/users/details`, {
            headers: {
              Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
          })
          .then(res => res.json())
          .then(data => {
            console.log(data)
            // Set the user states values with the user details upon successful login.
            if (typeof data._id !== "undefined") {

              setUser({
                id: data._id,
                isAdmin: data.isAdmin
              });

            // Else set the user states to the initial values
            } else {

              setUser({
                id: null,
                isAdmin: null
              });

            }

          })

          }, []);

      

  useEffect(() =>{
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (

    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
          
          
                <AppNavbar/>
                
                <Routes>
                  
                    <Route path="/" element={<Home/>} />
                    <Route path="/products" element={<Products/>} />
                    <Route path="/products/:productId" element={<ProductView user={user}/>} />
                    <Route path="/addProduct" element={<AddProduct/>} />
                    <Route path="/register" element={<Register/>} />
                    <Route path="/login" element={<Login/>} />
                    <Route path="/logout" element={<Logout/>} />
                    
                    <Route path="*" element={<Error />} />
                    <Route path="/profile" element={<Profile />} />

                    <Route path="/orders" element={<OrderHistory />} />
                    <Route path="/carts" element={<AddToCart user={user} />} />
                    
                  
                </Routes>
          
      </Router>
    </UserProvider>

  );
}



export default App;
