import React, { useState } from 'react';

const UpdateProfile = ({fetchUserProfile}) => {

  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');
  
  const handleSubmit = async (e) => {
    e.preventDefault();
    
    const token = localStorage.getItem('token'); // Replace this with the actual JWT token
    
    const data = {
      email
    };
    
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify(data)
      });
      
      if (response.ok) {
        setMessage('Profile udpated successfully');
        setEmail('');
        fetchUserProfile();
      } else {
        const errorData = await response.json();
        setMessage(errorData.message);
      }

    } catch (error) {
      setMessage('An error occurred. Please try again.');
      console.error(error);
    }
  };
  
  return (
    <div className="container mt-4 my-5">
      <h3>Update Profile</h3>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="email">Email:</label>
          <input
            type="email"
            className="form-control"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </div>
        
        <button type="submit" className="btn btn-success mt-3">Update Profile</button>
      </form>
      {message && <div className="mt-3 alert alert-info">{message}</div>}
    </div>
  );
};

export default UpdateProfile;
