import React, { useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import ProductCard from './ProductCard';

const SearchByPrice = () => {
    const [minPrice, setMinPrice] = useState('');
    const [maxPrice, setMaxPrice] = useState('');
    const [searchResults, setSearchResults] = useState([]);
    const [error, setError] = useState(null);
  
    const handleSearch = async () => {
      // Parse the prices and set them as numbers using setMinPrice and setMaxPrice
      const parsedMinPrice = minPrice !== '' ? parseFloat(minPrice) : '';
      const parsedMaxPrice = maxPrice !== '' ? parseFloat(maxPrice) : '';
  
      // Ensure that minPrice is less than or equal to maxPrice
      if (parsedMinPrice !== '' && parsedMaxPrice !== '' && parsedMinPrice > parsedMaxPrice) {
        alert('Min price cannot be greater than max price.');
        return;
      }
  
      try {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/products/search-by-price`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ minPrice: parsedMinPrice, maxPrice: parsedMaxPrice }),
        });
  
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
  
        const data = await response.json();
        setSearchResults(data.products);
        setError(null); // Clear any previous errors
      } catch (error) {
        console.error('Error searching for courses:', error);
        setError('An error occurred while fetching the data.');
      }
    };

  return (
    <Container fluid className="mt-5">
    <Row>
      <Col lg={{ span: 6, offset: 3 }}>
        
          <h3>Search Product by Price Range</h3>
          
            
              <div className="form-group">
                <label>Min Price:</label>
                <input
                  type="number"
                  className="form-control"
                  value={minPrice}
                  onChange={(e) => setMinPrice(e.target.value)}
                />
              </div>
            
           
              <div className="form-group pb-5">
                <label>Max Price:</label>
                <input
                  type="number"
                  className="form-control"
                  value={maxPrice}
                  onChange={(e) => setMaxPrice(e.target.value)}
                />
              </div>
            
          
          
            
              <button className="btn btn-dark" onClick={handleSearch}>
                Search
              </button>
            
         
              {searchResults.length > 0 ? (
                <div>
                  <h4>Search Results:</h4>
                  <ul>
                    {searchResults.map(product => (
                    <ProductCard productProp ={product} key={product._id}/>
                    ))}
                  </ul>
                </div>
              ) : (
                <p>No results found.</p>
              )}
           

        </Col>
          </Row>
        </Container>
      );
    };

export default SearchByPrice;
