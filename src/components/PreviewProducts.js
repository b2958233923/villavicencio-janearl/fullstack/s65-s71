import { Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Product(props) {

	const { breakPoint, data } = props

	const { _id, name, description, price, imgUrl } = data

	return (
	
		<Col xs={12} md={breakPoint}>
			<Card className="cardHighlight">
				<Card.Body>
				<Card.Img src={imgUrl} alt={name} className="product-image"/>
					<Card.Title className="text-center">
					<Link to={`/products/${_id}`}>{name}</Link>
					</Card.Title>
					<Card.Text>{description}</Card.Text>
				</Card.Body>

				<Card.Footer>
					<h5 className="text-center">{price}</h5>
					<Link to={`/products/${_id}`} className="btn btn-dark d-block">Details</Link>
				</Card.Footer>
			</Card>

		</Col>

	)
}