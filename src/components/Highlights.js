
import React from 'react';
import { Row, Col, Card, Container } from 'react-bootstrap';
import Carousel from 'react-bootstrap/Carousel';

export default function Highlights() {
  return (
    <div>
    <Container fluid className="d-flex justify-content-center align-items-center font">
      <Carousel>
        <Carousel.Item interval={1000}>
          <Row className='mt-3 mb-3'>
            <Col xs={12} md={4}>
              <Card className='cardHighlight p-3 text-center' style={{ width: '32rem' }}>
                <Card.Img style={{ width: '480px', height: '280px', objectFit: 'cover' }} variant="top" src="https://productimage001.runnersneed.com/productimages/989x1484/l1414412_5726_n.jpg" />
                <Card.Body>
                  <Card.Title>Adidas Adizero Boston 7</Card.Title>
                  <Card.Text>
                    Lightweight and responsive running shoe designed for neutral runners who want to get the most out of their speed
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
          
        </Carousel.Item>

        <Carousel.Item interval={1000}>
          <Row className='mt-3 mb-3'>
            <Col xs={12} md={4}>
              <Card className='cardHighlight p-3 justify-content-center' style={{ width: '32rem' }}>
                <Card.Img style={{ width: '480px', height: '255px', objectFit: 'cover' }} variant="top" src="https://images.asics.com/is/image/asics/DL408_0146_SR_RT_GLB-3?$sfcc-product$" />
                <Card.Body>
                  <Card.Title>Onitsuka Tiger Mexico 66</Card.Title>
                  <Card.Text>
                    Ideal for urban living, working and playing, this shoe is crafted from leather and boasts a heel-cross reinforcement for better stability and all-day comfort
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
          
        </Carousel.Item>

        <Carousel.Item interval={1000}>
          <Row className='mt-3 mb-3'>
            <Col xs={12} md={4}>
              <Card className='cardHighlight p-3' style={{ width: '32rem' }}>
                <Card.Img style={{ width: '480px', height: '280px', objectFit: 'cover' }} variant="top" src="https://static.nike.com/a/images/t_PDP_1280_v1/f_auto,q_auto:eco/dedfb816-6a3f-4cb7-9972-16d35cd5a916/alphafly-2-road-racing-shoes-cVPHCD.png" />
                <Card.Body>
                  <Card.Title>Nike Alphafly Next% 2</Card.Title>
                  <Card.Text>
                    Super shoe which was designed for narrow-footed runners and runners with high arches because it has a very narrow midfoot.
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
         
        </Carousel.Item>
      </Carousel>
    </Container>
    </div>
  );
}


