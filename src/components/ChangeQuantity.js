import React, { useState } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';

const ChangeQuantity = ({ cartItem, onUpdateQuantity }) => {
  const [showModal, setShowModal] = useState(false);
  const [quantity, setQuantity] = useState(cartItem.quantity);

  const handleEditQuantity = () => {
    setShowModal(true);
  };

  const handleModalClose = () => {
    setShowModal(false);
  };

  const handleQuantityChange = (event) => {
    setQuantity(parseInt(event.target.value, 10));
  };

  const handleSaveChanges = () => {
    onUpdateQuantity(cartItem.productId, quantity);
    setShowModal(false);
  };

  return (
    <>
      <Button variant="dark" className ="mx-1" onClick={handleEditQuantity}>
        Change Quantity
      </Button>

      <Modal show={showModal} onHide={handleModalClose}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Quantity</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Group>
            <Form.Label>Quantity</Form.Label>
            <Form.Control type="number" min="1" value={quantity} onChange={handleQuantityChange} />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleModalClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleSaveChanges}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ChangeQuantity;
