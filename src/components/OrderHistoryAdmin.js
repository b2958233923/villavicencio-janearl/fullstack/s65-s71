import React, { useEffect, useState } from 'react';
import { Table } from 'react-bootstrap';

export default function OrderHistoryAdmin({ userOrdersData, allOrdersData }) {
  const [userOrders, setUserOrders] = useState([]);
  const [allOrders, setAllOrders] = useState([]);

  useEffect(() => {
    if (Array.isArray(userOrdersData)) {
      const userOrdersArr = userOrdersData.map((order) => (
        <tr key={order._id}>
          <td>{order._id}</td>
          <td>{order.userId}</td>
          <td>{order.products.length}</td>
          <td>PhP {order.totalAmount}</td>
          <td>{new Date(order.purchasedOn).toLocaleString()}</td>
        </tr>
      ));
      setUserOrders(userOrdersArr);
    }

    if (Array.isArray(allOrdersData)) {
      const allOrdersArr = allOrdersData.map((order) => (
        <tr key={order._id}>
          <td>{order._id}</td>
          <td>{order.userId}</td>
          <td>{order.products.length}</td>
          <td>PhP {order.totalAmount}</td>
          <td>{new Date(order.purchasedOn).toLocaleString()}</td>
        </tr>
      ));
      setAllOrders(allOrdersArr);
    }
  }, [userOrdersData, allOrdersData]);

  return (
    <div className="table-container">
      <h2 className="text-center my-4">Admin Dashboard - Order History</h2>

      <h3 className="text-center mt-4">All Orders</h3>
      {Array.isArray(allOrdersData) && allOrdersData.length > 0 ? (
        <Table striped bordered hover responsive>
          <thead className="text-center">
            <tr>
              <th>Order ID</th>
              <th>User ID</th>
              <th>No. of Products</th>
              <th>Total Amount</th>
              <th>Purchased Date</th>
            </tr>
          </thead>
          <tbody>{allOrders}</tbody>
        </Table>
      ) : (
        <p className="text-center">No orders found.</p>
      )}

      <style jsx>{`
        .table-container {
          margin: 0 auto;
          max-width: 800px;
        }
        table {
          font-size: 14px;
        }
        th {
          font-weight: bold;
          background-color: #f2f2f2;
        }
        td {
          text-align: center;
        }
        tr:nth-child(even) {
          background-color: #f9f9f9;
        }
        tr:hover {
          background-color: #f5f5f5;
        }
        .text-center {
          text-align: center;
        }
        .my-4 {
          margin-top: 2rem;
          margin-bottom: 2rem;
        }
        .mt-4 {
          margin-top: 1.5rem;
        }
      `}</style>
    </div>
  );
};
