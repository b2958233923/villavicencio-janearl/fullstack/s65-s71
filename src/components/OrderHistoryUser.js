// import React, { useEffect, useState } from 'react';
// import { Card, Row, Col } from 'react-bootstrap';

// export default function OrderHistoryUser({ ordersData }) {
//   const [orders, setOrders] = useState([]);

//   useEffect(() => {
//     if (Array.isArray(ordersData)) {
//       const ordersArr = ordersData.map((order) => (
//         <Col key={order._id} lg={6} xs={12} md={8}>
//           <Card className="mb-3">
//             <Card.Body>
//               <Card.Title>Order ID: {order._id}</Card.Title>
//               <Card.Text>User ID: {order.userId}</Card.Text>
//               <Card.Text>Name: {order.products[0].name}</Card.Text>
//               <Card.Text>No. of Products: {order.products.length}</Card.Text>
//               <Card.Text>Total Amount: PhP {order.totalAmount}</Card.Text>
//               <Card.Text>Purchased Date: {new Date(order.purchasedOn).toLocaleString()}</Card.Text>
//             </Card.Body>
//           </Card>
//         </Col>
//       ));
//       setOrders(ordersArr);
//     }
//   }, [ordersData]);

//   return (
//     <>
//       <h2 className="text-center p-3">Your Purchase History</h2>
//       {Array.isArray(ordersData) && ordersData.length > 0 ? (
//         <Row>{orders}</Row>
//       ) : (
//         <p className="text-center">No orders found.</p>
//       )}
//     </>
//   );
// }

import React, { useEffect, useState } from 'react';
import { Table } from 'react-bootstrap';

export default function OrderHistoryUser({ ordersData }) {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    if (Array.isArray(ordersData)) {
      const ordersArr = ordersData.map((order) => (
        <tr key={order._id}>
          <td>Order ID: {order._id}</td>
          <td>User ID: {order.userId}</td>
          <td>Name: {order.products[0].name}</td>
          <td>No. of Products: {order.products.length}</td>
          <td>Total Amount: PhP {order.totalAmount}</td>
          <td>Purchased Date: {new Date(order.purchasedOn).toLocaleString()}</td>
        </tr>
      ));
      setOrders(ordersArr);
    }
  }, [ordersData]);

  return (
    <div className="table-container">
      <h2 className="text-center p-3">Your Purchase History</h2>
      {Array.isArray(ordersData) && ordersData.length > 0 ? (
        <Table striped bordered hover responsive>
          <thead>
            <tr>
              <th>Order ID</th>
              <th>User ID</th>
              <th>Name</th>
              <th>No. of Products</th>
              <th>Total Amount</th>
              <th>Purchased Date</th>
            </tr>
          </thead>
          <tbody>{orders}</tbody>
        </Table>
      ) : (
        <p className="text-center">No orders found.</p>
      )}

      <style jsx>{`
        .table-container {
          margin: 0 auto;
          max-width: 1000px;
        }
        table {
          font-size: 14px;
        }
        th {
          font-weight: bold;
          text-align: center;
          background-color: #f2f2f2;
        }
        td {
          text-align: center;
        }
        tr:nth-child(even) {
          background-color: #f9f9f9;
        }
        tr:hover {
          background-color: #f5f5f5;
        }
      `}</style>
    </div>
  );
};

