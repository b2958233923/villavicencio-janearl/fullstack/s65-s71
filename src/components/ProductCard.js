import React from 'react';
import { Row, Col, Card, Button, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import '../App.css';

export default function ProductCard({ productProp }) {
  const { _id, name, description, price, imgUrl } = productProp;

  return (
    <Card id='productComponent1' className="cardHighlight p-3">
      <Container>
        <Row>
          <Col xs={12} md={4}>
            {imgUrl && (
              <div className="image-container">
                {/* Add the 'product-image' class to the Card.Img */}
                <Card.Img src={imgUrl} alt={name} className="product-image" />
              </div>
            )}
          </Col>
          <Col xs={12} md={8}>
            <Card.Body>
              <Card.Title className="mb-3 text-primary">{name}</Card.Title>
              <Card.Subtitle className="mb-3">Description:</Card.Subtitle>
              <Card.Text className="mb-3">{description}</Card.Text>
              <Card.Subtitle className="mb-3">Price:</Card.Subtitle>
              <Card.Text className="text-danger mb-3">&#8369; {price}</Card.Text>
              <Button className="view-details" as={Link} to={`/products/${_id}`} variant="dark">
                View Details
              </Button>
            </Card.Body>
          </Col>
        </Row>
      </Container>
    </Card>
  );
}
