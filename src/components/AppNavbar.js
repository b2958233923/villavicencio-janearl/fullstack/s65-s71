import { useContext } from 'react';
import {Navbar, Nav, Container} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
// import UserContext from '../UserContext';
import UserContext from '../UserContext';




export default function AppNavbar() {

    // const [user, setUser] = useState(localStorage.getItem("token"));

    const{user} = useContext(UserContext);
    console.log(user);

    return (
    <Navbar expand="lg" className="custom-navbar font">
      <Container fluid>
        <Navbar.Brand as={Link} to="/">Sneakyz!</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Link as={NavLink} to="/" spy="true">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/products">On Sale!</Nav.Link>


            {
              (user.id !== null)?
              user.isAdmin
              ?
                <>
                  <Nav.Link as={NavLink} to="/addProduct">Add Product</Nav.Link>
                  <Nav.Link as={NavLink} to="/orders">
                    Order History
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                </>
                :
                <>
                  <Nav.Link as={NavLink} to="/profile">Profile</Nav.Link>
                  <Nav.Link as={NavLink} to="/carts">My Cart</Nav.Link>
                  <Nav.Link as={NavLink} to="/orders">
                    Order History
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                </>
                :
                <>
                  <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                  <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                </>
            }

          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>

    )
};

