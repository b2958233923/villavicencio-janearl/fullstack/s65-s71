import React, { useState, useEffect } from 'react';

import { Table } from 'react-bootstrap';
// import { Link } from 'react-router-dom';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';
import DeleteProduct from './DeleteProduct';
// import Swal from 'sweetalert2';

export default function AdminDashboard({ productsData, fetchData }) {
  const [products, setProducts] = useState([]);

  useEffect(() => {

    const productsArr = productsData.map((product) => {
      console.log(product)
      return (

        <tr key={product._id}>
          <td>{product._id}</td>
          <td>{product.name}</td>
          <td>{product.description}</td>
          <td>{product.price}</td>
          <td className={product.isActive ? 'text-success' : 'text-danger'}>
            {product.isActive ? 'Available' : 'Unavailable'}
          </td>
          <td>
            <img src={product.imgUrl} alt={product.name} style={{width: '100px'}} />
          </td>
          <td className="pt-4">
            <EditProduct product={product._id} fetchData={fetchData} />
          </td>
          <td className="pt-4">
            <ArchiveProduct product={product._id} isActive={product.isActive} fetchData={fetchData} />
          </td>
          <td className="pt-4">
            <DeleteProduct productId={product._id} isActive={product.isActive} fetchData={fetchData} />
          </td>
        </tr>
      );
    });

    setProducts(productsArr)

  }, [productsData, fetchData])



  return (
    <>
      <h2 className="text-center my-4">Admin Dashboard</h2>
    
      <Table striped bordered hover responsive>
        <thead className="text-center">
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Availability</th>
            <th className="text-center">Image</th>
            <th colSpan="3">Actions</th>
          </tr>
        </thead>
        <tbody>{products}</tbody>
      </Table>
    </>
  );
}
