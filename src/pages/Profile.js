import {useState,useEffect, useContext} from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import ResetPassword from '../components/ResetPassword';
import UpdateProfile from '../components/UpdateProfile';

export default function Profile(){

    const {user} = useContext(UserContext);

    const [details,setDetails] = useState({})

	const fetchUserProfile = () => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			// Set the user states values with the user details upon successful login.
			if (typeof data._id !== "undefined") {

				setDetails(data);

			}
        });

	}

    useEffect(()=>{

        fetchUserProfile();
    },[])

	return (
        // (user.email === null) ?
        // <Navigate to="/courses" />
        // :
        (user.id === null) ?
        <Navigate to="/products" />
        :
        <Container style={{ maxWidth: "800px" }}>
					<Row>
						<Col className="p-5 bg-secondary text-white">
							<h1 className="my-5 ">Profile</h1>
			                {/* <h2 className="mt-3">James Dela Cruz</h2> */}
							<h2 className="mt-3">{`${details.email}`}</h2>
							<hr />
							<h4>Contacts</h4>
							<ul>
								{/* <li>Email: {user.email}</li> */}
			                    <li>Email: {details.email}</li>
							</ul>
						</Col>
					</Row>

					<Row className='pt-4 mt-4'>
						<Col>
							<UpdateProfile fetchUserProfile={fetchUserProfile}/>
							<ResetPassword/>
						</Col>
					</Row>
		</Container>	
	)

}
