import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import OrderHistoryAdmin from '../components/OrderHistoryAdmin';
import OrderHistoryUser from '../components/OrderHistoryUser';

export default function OrderHistory() {
  const { user } = useContext(UserContext);

  // State that will be used to store the user's order history and all orders for admins.
  const [userOrders, setUserOrders] = useState([]);
  const [allOrders, setAllOrders] = useState([]);

  // Create a function to fetch the user's order history
  const fetchUserOrders = () => {
    // Check if the user is logged in
    if (!user.id) {
      return; // If the user is not logged in, do not make the API call
    }

    // Fetch the user's order history from the backend
    fetch(`${process.env.REACT_APP_API_URL}/orders/user-orders`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setUserOrders(data.orders); // Set the user's order history in the state
      })
      .catch((error) => {
        console.error(error);
      });
  };

  // Create a function to fetch all orders for the admin
  const fetchAllOrders = () => {
    // Fetch all orders from the backend
    fetch(`${process.env.REACT_APP_API_URL}/orders/all-orders`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setAllOrders(data.orders); // Set all orders in the state
      })
      .catch((error) => {
        console.error(error);
      });
  };

  // Fetch the user's order history and all orders for admins upon the initial render of the component
  useEffect(() => {
    fetchUserOrders();
    fetchAllOrders();
  }, [user.id]); // Fetch the orders again if the user ID changes (e.g., when the user logs in or logs out)

  return (
    <>
      {user && user.isAdmin ? (
        // If the user is an admin, show the Admin Dashboard with all orders.
        <OrderHistoryAdmin userOrdersData={userOrders} allOrdersData={allOrders} />
      ) : (
        // If the user is not an admin, show the User View with the user's order history.
        <OrderHistoryUser ordersData={userOrders} />
      )}
    </>
  );
}
