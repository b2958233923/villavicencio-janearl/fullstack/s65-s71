import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Register() {

	const { user } = useContext(UserContext);

	// State hooks to store the values of the register form input fields.

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");

	// State to determine whether the submit button is enabled or not.
	const [ isActive, setIsActive] = useState(false);

	// check if the values are successfully binded.

	console.log(email);
	console.log(password);
	console.log(confirmPassword);


	function registerUser(e) {

		// Prevents the page redirection via form submission
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data) {
				alert("Duplicate email found!")
			} else {

				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						email: email,
						password: password
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data) {
						setEmail("");
						setPassword("");
						setConfirmPassword("");

						alert("Thank you for registering!")

					} else {

						alert("Please try again later.")
					}

				})
			}


		})

	};




	/*
		This side effect is to validate the states whether each state is empty string or not, if password is correct and if mobileNo has 11 digits.
	*/
	useEffect(() => {

		if(( email !== ""  && password !== "" && confirmPassword !== "") && (password === confirmPassword)) {
				setIsActive(true)
		} else {

				setIsActive(false)
		}

	}, [ email, password, confirmPassword]);



	return (
		
		(user.id !== null) ?
			<Navigate to="/products" />
			:
			<Container className="font" style={{ maxWidth: "400px" }}>
		<Form onSubmit={e => registerUser(e)}>
		  <h1 className="my-5 text-center">Register</h1>

		  

		  

		  <Form.Group className="mb-3" controlId="Email address">
		    <Form.Label>Email address</Form.Label>
		    <Form.Control 
		    	type="email" 
		    	placeholder="name@example.com"  
		    	required
		    	value={email}
		    	onChange={e => {setEmail(e.target.value)}}
		    />
		  </Form.Group>

		  

		  <Form.Group className="mb-3" controlId="Password1">
		    <Form.Label>Password</Form.Label>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Enter Password" 
		    	required 
		    	value={password}
		    	onChange={e => {setPassword(e.target.value)}}
		    />
		  </Form.Group>

		  <Form.Group className="mb-3" controlId="Password2">
		    <Form.Label>Confirm Password:</Form.Label>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Confirm Password" 
		    	required 
		    	value={confirmPassword}
		    	onChange={e => {setConfirmPassword(e.target.value)}}
		    />
		  </Form.Group>

		  {
		  	isActive 
		  		? <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
		  		: <Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
		  }

		</Form>
		
		
</Container>


	)

};