import {useEffect, useState, useContext } from 'react';
import { Container } from 'react-bootstrap';
import UserContext from '../UserContext';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';
import SearchByPrice from '../components/SearchByPrice';

	

export default function Products() {

	const { user } = useContext(UserContext);

	
	const [products, setProducts] = useState([])

	
	const fetchData = () => {
		
		
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(res => res.json())
		.then(data =>{
			console.log(data)

			
			setProducts(data);
		});

	}


	
	useEffect(() =>{

		fetchData();

	},[]);

	return (
		<>
	    <Container className="font">
		<SearchByPrice/>
	      {user.isAdmin ? (
	        
	        <AdminView productsData={products} fetchData={fetchData}/>
	      ) : (
	        
	        <UserView productsData={products} />
	      )}
		  
	    </Container>
		<footer className= "bg-dark text-light text-center pt-1">
            <div className="container">
                <div className="col">
                    <p>
                        2023
                        Sneakyz! All Rights Reserved.
                    </p>
                    <p>
                        Made by JEHV
                    </p>
                </div>
            </div>
        </footer>
		</>
	  );

};