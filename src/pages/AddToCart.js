// import React, { useState, useEffect, useContext } from 'react';
// import UserContext from '../UserContext';

// import AddToCartUser from '../components/AddToCartUser';

// export default function AddToCart() {
//   const { user } = useContext(UserContext);

//   // State that will be used to store the user's order history and all orders for admins.
//   const [userCarts, setUserCarts] = useState([]);
  

//   // Create a function to fetch the user's order history
//   const fetchUserCarts = () => {
    // Check if the user is logged in
  //   if (!user.id) {
  //     return; // If the user is not logged in, do not make the API call
  //   }

  //   // Fetch the user's order history from the backend
  //   fetch(`${process.env.REACT_APP_API_URL}/carts/`, {
  //     headers: {
  //       Authorization: `Bearer ${localStorage.getItem('token')}`,
  //     },
  //   })
  //     .then((res) => res.json())
  //     .then((data) => {
  //       console.log(data);
  //       setUserCarts(data.carts); // Set the user's order history in the state
  //     })
  //     .catch((error) => {
  //       console.error(error);
  //     });
  // };

//   // Create a function to fetch all orders for the admin
 

//   // Fetch the user's order history and all orders for admins upon the initial render of the component
//   useEffect(() => {
//     fetchUserCarts();
    
//   }, [user.id]); // Fetch the orders again if the user ID changes (e.g., when the user logs in or logs out)

//   return (
    
        
     
        
//         <AddToCartUser cartsData={userCarts} />
      
    
//   );
// }




// import React, { useState, useEffect, useContext } from 'react';
// import UserContext from '../UserContext';
// import AddToCartUser from '../components/AddToCartUser';

// export default function AddToCart() {
//   const { user } = useContext(UserContext);
//   const [userCarts, setUserCarts] = useState([]);

//   const fetchUserCarts = () => {
//     // Check if the user is logged in
//     if (!user.id) {
//       return; // If the user is not logged in, do not make the API call
//     }

//     // Fetch the user's order history from the backend
//     fetch(`${process.env.REACT_APP_API_URL}/carts/all-user-cart`, {
//       headers: {
//         Authorization: `Bearer ${localStorage.getItem('token')}`,
//       },
//     })
//       .then((res) => res.json())
//       .then((data) => {
//         setUserCarts(data.carts); // Set the user's order history in the state
//       })
//       .catch((error) => {
//         console.error(error);
//       });
//   };

//   useEffect(() => {
//     fetchUserCarts();
//   }, [user.id]);

//   return (
//     <>
//       <AddToCartUser cartsData={userCarts} />
//     </>
//   );
// }

import React, { useState, useEffect } from 'react';
import { Container, Table, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
// import UserContext from '../UserContext';
import ChangeQuantity from '../components/ChangeQuantity';
import RemoveProduct from '../components/RemoveProduct';
import Swal from 'sweetalert2';

const ManageCart = ({ user }) => {
  const [cartDetails, setCartDetails] = useState({ itemsSubtotal: [], totalPrice: 0 });
  const [isLoading, setIsLoading] = useState(true);
  const [showModal, setShowModal] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null)

  useEffect(() => {
    if (!user.isAdmin) {
      fetchCartDetails();
    } else {
      setIsLoading(false);
    }
  }, [user.isAdmin]);

  const fetchCartDetails = () => {
    fetch(`${process.env.REACT_APP_API_URL}/carts/getCartSubtotal`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setCartDetails(data);
        setIsLoading(false);
      })
      .catch((error) => {
        console.error('Error fetching cart details:', error);
        setIsLoading(false);
      });
  };

  const handleUpdateQuantity = (productId, quantity) => {
     // Send the PUT request to update the product quantity
     fetch(`${process.env.REACT_APP_API_URL}/carts/changeQuantity/${productId}`, {
       method: 'PUT',
       headers: {
         'Content-Type': 'application/json',
         Authorization: `Bearer ${localStorage.getItem('token')}`,
       },
       body: JSON.stringify({ quantity }),
     })
       .then((res) => res.json())
       .then((data) => {
         console.log(data); // Success message or other response data
         // After updating the quantity, you may want to fetch the updated cart details again
         fetchCartDetails();

         Swal.fire({
          title: 'Quantity Updated',
          icon: 'success',
          text: 'The quantity has been updated successfully!',
        })
       })
       .catch((error) => {
         console.error('Error updating cart quantity:', error);
         // Handle error or show error message to the user
       });
   };

   const handleShowModal = (product) => {
       setSelectedProduct(product);
       setShowModal(true);
     };

     const handleRemoveProduct = (productId) => {
       fetch(`${process.env.REACT_APP_API_URL}/carts/removeProduct`, {
         method: 'DELETE',
         headers: {
           'Content-Type': 'application/json',
           Authorization: `Bearer ${localStorage.getItem('token')}`,
         },
         body: JSON.stringify({ productId }),
       })
         .then((res) => res.json())
         .then((data) => {
           if (data) {
             Swal.fire({
               title: 'Product Removed',
               icon: 'success',
               text: 'The product has been removed from your cart.',
             });
             fetchCartDetails(); // Fetch updated cart details after successful removal
           } else {
             Swal.fire({
               title: 'Product Removal Failed',
               icon: 'error',
               text: 'Failed to remove the product from your cart.',
             });
           }
         })
         .catch((error) => {
           console.error('Error removing product from cart:', error);
           // Handle error or show error message to the user
         });
     };

   if (user.isAdmin) {
    return <Navigate to="/home" />;
  }

  return (
    <Container className="mt-5 min-vh-100">
    <h1 className="text-center font">Sneakyz Cart</h1>
    {isLoading ? (
      <p>Loading...</p>
    ) : (
      <Container className="text-center">
        {!cartDetails.itemsSubtotal || cartDetails.itemsSubtotal.length === 0 ? (
          <p>No items in cart found.</p>
        ) : (
          <div className="table-responsive">
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Product ID</th>
                  <th>Product Name</th>
                  <th>Quantity</th>
                  <th>Item Subtotal</th>
                  <th>Edit Quantity/Remove Product</th>
                </tr>
              </thead>
              <tbody>
                {cartDetails.itemsSubtotal.map((item) => (
                  <tr key={item.productId}>
                    <td>{item.productId}</td>
                    <td>{item.productName}</td>
                    <td>{item.quantity}</td>
                    <td>&#8369; {item.itemSubtotal}</td>
                    <td>
                      <ChangeQuantity cartItem={item} onUpdateQuantity={handleUpdateQuantity} />
                      <Button variant="danger" onClick={() => handleShowModal(item)} className="mx-1">
                        Remove
                      </Button>
                    </td>
                  </tr>
                ))}
                <RemoveProduct
                  show={showModal}
                  onHide={() => setShowModal(false)}
                  product={selectedProduct}
                  onRemoveProduct={handleRemoveProduct}
                />

                <tr>
                  <td colSpan="3" className="text-center">Total Amount:</td>
                  <td colSpan="2" className="text-center">&#8369; {cartDetails.totalPrice}</td>
                </tr>
              </tbody>
            </Table>
          </div>
        )}
      </Container>
    )}
  </Container>
  );
};

export default ManageCart;


