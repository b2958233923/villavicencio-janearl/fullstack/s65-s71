import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function AddProduct() {

	const { user } = useContext(UserContext);
  
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [imgUrl, setImgUrl] = useState('');
  const [isActive, setIsActive] = useState(false);

  const navigate = useNavigate();

  function addProduct(e) {
    e.preventDefault();

    const token = localStorage.getItem('token');

    fetch(`${process.env.REACT_APP_API_URL}/products/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
		imgUrl: imgUrl
      })
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);

        if(data){
			Swal.fire({
				title:"New Product Added",
				icon: "success",
				text: "You have successfully added this product."
			})

			
			navigate("/products")
		} else {
			Swal.fire({
				title:"Unsuccessful Product Creation",
				icon: "error",
				text: "Something went wrong. Please try again."
			})
		}

        setName("");
        setDescription("");
        setPrice("");
		setImgUrl("");
      })
  }

    useEffect(() => {
    setIsActive(name !== "" && description !== "" && price !== "" && imgUrl !== "");
  }, [name, description, price, imgUrl]);

		  return (

		  	(user.isAdmin === true) ?
		  		
		  	<Container style={{ maxWidth: "800px" }}>	
			    <Form onSubmit={addProduct}>
				    <h1 className="my-5 text-center">Add Product</h1>

				      <Form.Group className="mb-3" controlId="Name">
				        <Form.Label>Name:</Form.Label>
				        <Form.Control
				          type="text"
				          placeholder="Enter Name"
				          value={name}
				          onChange={e => setName(e.target.value)}
				        />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="Description">
				        <Form.Label>Description:</Form.Label>
				        <Form.Control
				          type="text"
				          placeholder="Enter Description"
				          value={description}
				          onChange={e => setDescription(e.target.value)}
				        />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="Price">
				        <Form.Label>Price:</Form.Label>
				        <Form.Control
				          type="text"
				          placeholder="Enter Price"
				          value={price}
				          onChange={e => setPrice(e.target.value)}
				        />
				      </Form.Group>

					  <Form.Group className="mb-3" controlId="Img">
				        <Form.Label>Image:</Form.Label>
				        <Form.Control
				          type="text"
				          placeholder="Put Image URL"
				          value={imgUrl}
				          onChange={e => setImgUrl(e.target.value)}
				        />
				      </Form.Group>
					  
				      			{
					               isActive
					                    ? <Button variant="success" type="submit" id="submitBtn">Submit</Button>
					                    : <Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
					            }
			    </Form>
		    </Container>
		    :
		    <Navigate to="/products" />
		  );
		}
