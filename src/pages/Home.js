import { Container } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLinkedinIn, faFacebook, faGithub, faTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import FeaturedProducts from '../components/FeaturedProducts';

export default function Home() {
    const bannerData = {
        title: 'Sneakyz!',
        content: 'Sneakers/Runners Shoes for everyone, everywhere!',
        destination: '/products',
        label: 'Add to Cart now!',
    };

    return (
        <>
            <Banner data={bannerData} />
            <Highlights />
            <FeaturedProducts />
            <Container>
                <div className="font">
                    <h2 className="text-center pt-5">Featured Videos</h2>
                    <div className="embed-responsive embed-responsive-16by9">
                        <iframe
                        width="100%"
                            height="720"
                            className="embed-responsive-item"
                            src="https://www.youtube.com/embed/6Dd37hGwHCg"
                            title="On Cloudflow V3 | Runner's Review | #Fit2Run"
                            frameBorder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                            allowFullScreen
                        ></iframe>
                    </div>
                </div>
            </Container>

            <Container>
                <div>
                    <h1 className="text-center my-3 display-4">Contact Me</h1>

                    <div className="row justify-content-center">
                        <div className="col-md-6 my-4">
                            <h4 className="text-center my-4">Message Me</h4>
                            <form id="form-col" className="p-2">
                                <div className="form-group mt-2">
                                    <label htmlFor="exampleInputName1">Full Name</label>
                                    <input type="text" className="form-control" id="exampleInputName1" placeholder="Your Name" />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="exampleInputEmail1">Email address</label>
                                    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="name@example.com" />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="exampleInputContactNumber">Contact Number</label>
                                    <input type="text" className="form-control" id="exampleInputContactNumber" placeholder="+63 900 000 000" />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="exampleFormControlTextarea1">Message</label>
                                    <textarea className="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                </div>

                                <button type="button" className="btn btn-primary bg-dark" data-toggle="modal" data-target="#modalDiv">Send</button>
                            </form>
                        </div>
                    </div>

                    <div id="contact" className="row justify-content-center mb-5">
                        <div className="col-12 text-center">
                            <a href="https://www.linkedin.com" target="_blank">
                                <FontAwesomeIcon icon={faLinkedinIn} />
                            </a>
                            <a href="https://www.facebook.com" target="_blank">
                                <FontAwesomeIcon icon={faFacebook} />
                            </a>
                            <a href="https://github.com/" target="_blank">
                                <FontAwesomeIcon icon={faGithub} />
                            </a>
                            <a href="https://www.twitter.com" target="_blank">
                                <FontAwesomeIcon icon={faTwitter} />
                            </a>
                            <a href="https://www.instagram.com" target="_blank">
                                <FontAwesomeIcon icon={faInstagram} />
                            </a>
                        </div>
                    </div>
                </div>
            </Container>

            <footer className="bg-dark text-light text-center pt-1">
                <div className="container">
                    <div className="col">
                        <p>
                            {new Date().getFullYear()} Sneakyz! All Rights Reserved.
                        </p>
                        <p>
                            Made by JEHV
                        </p>
                    </div>
                </div>
            </footer>
        </>
    );
}
